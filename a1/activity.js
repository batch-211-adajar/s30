
db.fruits.aggregate([
        {$match: {onSale: true}},
	    {$group: {_id: "$name"}},
        {$count: "fruitsOnSale"}
	
	]);


db.fruits.aggregate([
        {$match: {stock: {$gt: 20}}},
		{$group: {_id: "$name"}},
        {$count: "enoughStock"}
	
	]);




db.fruits.aggregate([
        {$match: {onSale: true}},
		{$group: {_id: "$price"}},
        avgPriceOnSale: {$avg: "$price"}        
	]);


db.fruits.aggregate([
	{$group: {_id: "$price"}},
	 maxPrice: { $max: "$price" }

	]);

db.fruits.aggregate([
	{$group: {_id: "$price"}},
	 minPrice: { $min: "$price" }
	 
	]);